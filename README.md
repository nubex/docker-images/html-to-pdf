#### PDF generator from HTML by wkhtmltopdf

PHP example:
```
$context = stream_context_create([
    'http' => [
        'method'  => 'POST',
        'header'  => 'Content-Type: application/json',
        'content' => json_encode([
            'contents' => base64_encode($html),
            'options' => [
                // options from "wkhtmltopdf --help"
                'margin-top' => '10mm',
                'margin-bottom' => '20mm',
                'dpi' => '1000'
            ],
        ])
    ]
]);

$filename = 'http://pdf';
$file = file_get_contents($filename, null, $context);
```

### Use in docker-compose.yml
```
pdf:
    image: registry.gitlab.com/nubex/html-to-pdf:latest
    restart: always
```